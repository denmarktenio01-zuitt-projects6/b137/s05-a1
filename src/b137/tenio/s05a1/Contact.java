package b137.tenio.s05a1;

import java.util.ArrayList;

public class Contact {
    private String name;

    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    public Contact() {

    }

    public Contact(String name,String number, String address) {
        this.name = name;
        numbers.add(number);
        addresses.add(address);
    }

    // getters


    public String getName() {
        return name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    // setters


    public void setName(String name) {
        this.name = name;
    }

    public void setNumbers(String number) {
        numbers.add(number);
    }

    public void setAddresses(String address) {
        addresses.add(address);
    }
}
