package b137.tenio.s05a1;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        contact1.setName("Totoy Mola");
        contact1.setNumbers("09099990909");
        contact1.setNumbers("0900999000");
        contact1.setAddresses("Taguig");
        contact1.setAddresses("Cotabato");

        Contact contact2 = new Contact();
        contact2.setName("Totoy Bato");
        contact2.setNumbers("09000009999");
        contact2.setNumbers("0999999000");
        contact2.setAddresses("Manila");
        contact2.setAddresses("Cainta");

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.checkPhonebook();

    }
}
