package b137.tenio.s05a1;

import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {

    }

    public Phonebook(Contact contact) {
        contacts.add(contact);
    }

    // getters

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // setter
    public void setContacts(Contact contact) {
        contacts.add(contact);
    }

    // method
    public void checkPhonebook() {
        if (contacts.isEmpty()){
            System.out.println("This phonebook is empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getNumbers()) {
                    System.out.println(number);
                }
                System.out.println("--------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }
                System.out.println("====================");
            }
        }
    }
}
